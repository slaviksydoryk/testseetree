export default {
  colors: {
   primary: '#000',
  },
  fonts: {
    regular: 'Roboto'
  },
  sizes: {
    fontSize: 24,
  }
}