import React, { Component, Fragment } from 'react';
import { ThemeProvider } from 'styled-components';
import { BrowserRouter } from 'react-router-dom';

import { LightTheme } from './styles/themes'; // in future can make theme switcher
import GlobalStyles from './styles/globalStyles';

import Root from './routes/root';

class App extends Component {
  render() {
    return (
     <BrowserRouter>
      <ThemeProvider theme={LightTheme}>
        <Fragment>
          <Root/>
          <GlobalStyles />
        </Fragment>
      </ThemeProvider>
     </BrowserRouter>
    );
  }
}

export default App;
