/* String -> string || undefined */
export const getItem = k => localStorage.getItem(k);

/* String, Any-> null */
export const setItem = (k, v) => localStorage.setItem(k, v);

/* String-> null */
export const removeItem = k => localStorage.removeItem(k);