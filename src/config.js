export const appName = 'test-task-map';

export const apiUrl = process.env.API_URL || 'http://localhost:8080/api';

export const MAP_ACCESS_TOKEN = process.env.ACCESS_KEY || 'pk.eyJ1IjoibWtvemFrMDEwOCIsImEiOiJjamljeGF4Y3MwNGZrM3BvNmMxd2hsY2wzIn0.ErvzY580HhOEokFZR1GOTg';

export const MAP_STYLES_URL = 'mapbox://styles/mapbox/streets-v9';
