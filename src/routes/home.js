import React from 'react';

import MapComponent from '../components/map';

const Main = () => (
  <MapComponent />
);

export default Main;
