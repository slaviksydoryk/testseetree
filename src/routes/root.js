import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Home from './home';

const Root = () => (
  <Switch>
    <Route path="/" exact component={Home} />
  </Switch>
);

export default Root;