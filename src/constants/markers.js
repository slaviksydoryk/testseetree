export const MARKER_TYPES = [
  {
    title: 'Black',
    color: '#000',
    score: 0
  },
  {
    title: 'White',
    color: '#bcbcbc',
    score: 1
  },
  {
    title: 'Red',
    color: '#ff090c',
    score: 2
  },
  {
    title: 'Orange',
    color: '#fb9520',
    score: 3
  },
  {
    title: 'Lime',
    color: '#46a100',
    score: 4
  },
  {
    title: 'Green',
    color: '#129643',
    score: 5
  }
];