import ReactMap from "react-mapbox-gl";

import { MAP_ACCESS_TOKEN } from '../../config';

export default ReactMap({
  accessToken: MAP_ACCESS_TOKEN,
});
