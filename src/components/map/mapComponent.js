import React, { Component, Fragment } from 'react';
import { Marker, Popup } from "mapbox-gl";
import styled from 'styled-components';

import map from './mapInizialization';

import { MAP_STYLES_URL } from '../../config';
import { MARKER_TYPES } from '../../constants/markers';

import MarkerStatistic from '../MarkerStatistic';

export const MapS = styled(map)`
  height: 100vh;
  width: 100vw;
`;

const getMarkerPopupChangeScores = (selectCallback, selected) => {
  const select = document.createElement('select');

  select.addEventListener('change', selectCallback);

  const options = MARKER_TYPES.map(({ title, score }) => {
    const option = document.createElement('option');
    option.value = score;
    option.innerText = title;

    return option;
  });

  options.forEach(o => select.appendChild(o));

  select.value = selected;

  return select;
};

class MapComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      markers    : [],
      markerPopup: null
    };

    this.initNewMarker = this.initNewMarker.bind(this);
  }
/*
* NOTE: i know that is bad code. But i can`t take access to component via mapbox lib for react
* and i should work here with real DOM not with Virtual (Raact DOM)
* */
  initNewMarker(map, e) {
    const popup = new Popup()
      .setLngLat(e.lngLat);

    const marker = new Marker({
      draggable: true,
      color: MARKER_TYPES[0].color,
    })
      .setLngLat(e.lngLat)
      .addTo(map)
      .setPopup(popup);

    //marker.on('click', () => console.log('here') ); - it isn`t works for me((
    // it is strange way of writing it like it is, but i can`t find in docs other solution to do event handling
    // btw this lib has not clear docs (maybe it for me as i write it a bit but anyway(((
    marker.getElement().ondblclick = e => {
      e.stopPropagation();

    this.setState({
      ...this.state,
      markers: this.state.markers.filter(m => m.ref !== marker),
    });

      marker.remove();
    };

    marker.getElement().onclick = clickEvent => {
      clickEvent.stopPropagation();

      marker.togglePopup();
    };

    const changeMarkerScore = changeEvent => {
      this.setState({
        ...this.state,
        markers: this.state.markers.map(m => m.ref === marker ? {...m, ...MARKER_TYPES[changeEvent.target.value]} : m)
      });
    };
    popup.setDOMContent(getMarkerPopupChangeScores(changeMarkerScore, MARKER_TYPES[0].score));

    this.setState({
      ...this.state,
      markers: [...this.state.markers, {
        ref: marker,
        ...MARKER_TYPES[0]
      }]
    })

  }

  render() {
    return (
      <Fragment>
        <MapS
          style={MAP_STYLES_URL}
          onClick={this.initNewMarker}
        >

        </MapS>
        <MarkerStatistic
          markers={this.state.markers}
        />
      </Fragment>
    );
  }
}

export default MapComponent;