import React from 'react';
import styled from 'styled-components';

import { MARKER_TYPES } from '../../constants/markers';

const StatisticWrapS = styled.div`
  width: 20%;
  height: auto;
  
  position: fixed;
  top: 3px;
  right: 3px;
  
  background-color: #fff;
`;

const DataRowS = styled.div`
  display: flex;
  justify-content: space-around;
`;

const MarkerStatistics = ({ markers }) => (
  <StatisticWrapS>
    <DataRowS>
      <span>Total</span>
      <span>{ markers.length }</span>
    </DataRowS>
    {
      MARKER_TYPES.map(({ title, score }) => (
        <DataRowS key={score}>
          <span>{ title }</span>
          <span>{ markers.filter(m => m.score === score).length }</span>
        </DataRowS>
      ))
    }
  </StatisticWrapS>);

export default MarkerStatistics;
